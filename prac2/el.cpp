#include <cc/diagnostics.hpp>
#include <cc/input.hpp>
#include <cc/output.hpp>

#include <iostream>
#include <fstream>
#include <iterator>


int_literal int_literal:: fold(){
	return int_literal(value);
}

expr arith_expr:: fold(){
	// fold left
	num_expr newLeft = left*.fold();

	//fold right
	num_expr newRight = right*.fold();

	//attempt to evaluate
	if (newLeft.num_expr_type == type_int &&
		newRight.num_expr_type == type_int ) {
		switch(arith_op){
			case op_add:
				return int_literal(newLeft.value + newRight.value);
			case op_sub:
				return int_literal(newLeft.value - newRight.value);			
			case op_mul:
				return int_literal(newLeft.value * newRight.value);			
			case op_div:
				//we have to handle this case
				if(newRight.value == 0)
					return err(DIV_ZERO);
				return int_literal(newLeft.value / newRight.value);			
			case op_rem:
				return int_literal(newLeft.value % newRight.value);			
		}
	}
}



