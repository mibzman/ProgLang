#include <algorithm>
#include <utility> 

#pragma once

enum logic_op {
	op_and,
	op_or,
};

enum rel_op {
	op_less,
	op_equal,
	op_greater,
};

enum arith_op {
	op_add,
	op_sub,
	op_mul,
	op_div,
	op_rem,
};

enum num_expr_type {
	type_int,
	type_arg,
	type_arith,
	type_if
};

enum bool_expr_type {
	type_bool,
	type_rel,
	type_logic
};

struct prog;
struct num_expr;
struct bool_expr;

struct prog {
	int num_args;
	num_expr* body;
};

struct expr {};

struct num_expr : expr{
	num_expr_type type;
};

struct bool_expr : expr{
	bool_expr_type type;
};

struct err : expr{
	enum error { 
		DIV_ZERO,
	};
};

struct int_literal :num_expr {
	int value;

	int_literal fold();
};

struct arith_expr :num_expr {
	arith_op op;
	num_expr* left;
	num_expr* right;

	arith_expr fold();
};

struct arg_expr : num_expr {
	int arg;
};

struct if_expr : num_expr {
	bool_expr* test;
	num_expr* pass;
	num_expr* fail;
};

struct bool_literal : bool_expr {
	bool value;
};

struct rel_expr : bool_expr {
	rel_op op;
	num_expr* left;
	num_expr* right;
};

struct logic_expr : bool_expr {
	logic_op op;
	bool_expr* left;
	bool_expr* right;
};

int nHeight(num_expr* exp);
int bHeight(bool_expr* exp);

int nHeight(num_expr* exp) {
	switch(exp->type){
		case type_int:
			return 0;
		case type_arg:
			return 0;
		case type_arith:
			return 1 + std::max(
				nHeight(static_cast<const arith_expr*>(exp)->left),
				nHeight(static_cast<const arith_expr*>(exp)->right));
		case type_if:
			return 1 + std::max(
				bHeight(static_cast<const if_expr*>(exp)->test),
				std::max(
					nHeight(static_cast<const if_expr*>(exp)->pass),
					nHeight(static_cast<const if_expr*>(exp)->fail)));				
	}
}

int bHeight(bool_expr* exp) {
	switch(exp->type){
		case type_bool:
			return 0;
		case type_rel:
			return 1 + std::max(
				nHeight(static_cast<const rel_expr*>(exp)->left),
				nHeight(static_cast<const rel_expr*>(exp)->right));
		case type_logic:
			return 1 + std::max(
				bHeight(static_cast<const logic_expr*>(exp)->left),
				bHeight(static_cast<const logic_expr*>(exp)->right));
	}
}

std::pair<int, int> min_max_args(bool_expr* arg);

std::pair<int, int> min_max_args(num_expr* arg) {
	std::pair<int, int> left, right;
	switch (arg->type) {
	case num_expr_type::type_int:
		return std::pair<int, int>(0, 0);
	case num_expr_type::type_arg:
		return std::pair<int, int>(static_cast<arg_expr*>(arg)->arg, static_cast<arg_expr*>(arg)->arg);
	case num_expr_type::type_arith:
		left = min_max_args(static_cast<arith_expr*>(arg)->left);
		right = min_max_args(static_cast<arith_expr*>(arg)->right);
		return std::pair<int, int>(
			std::min(left.first, right.first), 
			std::max(left.second, right.second)
			);
	case num_expr_type::type_if:
		left = min_max_args(static_cast<if_expr*>(arg)->pass);
		right = min_max_args(static_cast<if_expr*>(arg)->fail);
		std::pair<int, int> test = min_max_args(static_cast<if_expr*>(arg)->test);
		return std::pair<int, int>(
			std::min(test.first, std::min(left.first, right.first)), 
			std::max(test.second, std::max(left.second, right.second))
			);
	}
}

std::pair<int, int> min_max_args(bool_expr* arg) {
	std::pair<int, int> left, right;
	switch (arg->type) {
	case bool_expr_type::type_bool:
		return std::pair<int, int>(0, 0);
	case bool_expr_type::type_rel:
		left = min_max_args(static_cast<rel_expr*>(arg)->left);
		right = min_max_args(static_cast<rel_expr*>(arg)->right);
		return std::pair<int, int>(
			std::min(left.first, right.first), 
			std::max(left.second, right.second)
			);
	case bool_expr_type::type_logic:
		left = min_max_args(static_cast<logic_expr*>(arg)->left);
		right = min_max_args(static_cast<logic_expr*>(arg)->right);
		return std::pair<int, int>(
			std::min(left.first, right.first), 
			std::max(left.second, right.second)
			);
	}
}