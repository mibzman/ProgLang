
#include <iostream>
#include <fstream>
#include <iterator>

enum ExprKind : int {
  // Pure lambda calculus
  ek_ref,
  ek_abs,
  ek_app,

  // Basic boolean expressions
  ek_bool,
  ek_cond,

  // Syntactic sugar
  ek_ph,
  ek_lambda,
  ek_call,
  ek_bind,
  ek_let,
  ek_seq,
};

class Expression{
  public:
    ExprKind type;

    int val;
    std::string name;
    Expression * body;
    Expression * arg;

    Expression(ExprKind t) : type(t), name(), body(), arg() {}
};

