package main

import ()

type NumericExpression interface {
	NHeight() int
}

//go will automatically pick up that intliteral implements
//numeric expression
type IntLiteral struct {
	Value int
}

func (IntLiteral) NHeight() int {
	return 0
}

type ArgumentExpression struct {
	Argument int
}

func (ArgumentExpression) NHeight() int {
	return 0
}

type ArithmeticExpression struct {
	Operation Arithmetic
	Left      NumericExpression
	Right     NumericExpression
}

func (a ArithmeticExpression) NHeight() int {
	return 1 + max(a.Left.NHeight(), a.Right.NHeight())
}

type IfExpression struct {
	Test BooleanExpression
	Pass NumericExpression
	Fail NumericExpression
}

func (If IfExpression) NHeight() int {
	return 1 + max(If.Test.BHeight(), max(If.Fail.NHeight(), If.Pass.NHeight()))
}

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}
