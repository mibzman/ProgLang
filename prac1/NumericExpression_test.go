package main

import (
	"github.com/stretchr/testify/assert"
	// "log"
	"testing"
)

func TestHeight(t *testing.T) {
	assert := assert.New(t)

	TestNumericExpression := IfExpression{
		Test: RelationalExpression{
			Operation: LessThan,
			Left:      ArgumentExpression{1},
			Right:     IntLiteral{0},
		},
		Pass: IntLiteral{-1},
		Fail: IntLiteral{-1},
	}

	assert.Equal(2, TestNumericExpression.NHeight(), "Wrong Height")

	TestNumericExpression = IfExpression{
		Test: RelationalExpression{
			Operation: LessThan,
			Left:      TestNumericExpression,
			Right:     IntLiteral{0},
		},
		Pass: IntLiteral{-1},
		Fail: IntLiteral{-1},
	}

	assert.Equal(4, TestNumericExpression.NHeight(), "Wrong Height")
}
