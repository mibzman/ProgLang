package main

import ()

//this is go's version of enum
type Arithmetic int

const (
	Add Arithmetic = 1 + iota
	Sub
	Mult
	Div
	Rem
)

type Relational int

const (
	LessThan Relational = 1 + iota
	Equal
	GreaterThan
)

type Logical int

const (
	And Logical = 1 + iota
	Or
)

type Program struct {
	Args int
	Body NumericExpression
}

func main() {
}
