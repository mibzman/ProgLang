package main

import ()

type BooleanExpression interface {
  BHeight() int
}

type BooleanLiteral struct {
  Value bool
}

func (BooleanLiteral) BHeight() int {
  return 0
}

type RelationalExpression struct {
  Operation Relational
  Left      NumericExpression
  Right     NumericExpression
}

func (a RelationalExpression) BHeight() int {
  return 1 + max(a.Left.NHeight(), a.Right.NHeight())
}

type LogicalExpression struct {
  Operation Logical
  Left      BooleanExpression
  Right     BooleanExpression
}

func (a LogicalExpression) BHeight() int {
  return 1 + max(a.Left.BHeight(), a.Right.BHeight())
}
